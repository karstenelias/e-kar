_Karsten Roth_ | _Elias Eulig_
---
# README
---
## This Repository contains all code used in our Self-Driving Car IAI 2017 project.
### It is self-contained and uses only relative pathing to allow for easy replication.
---

To successfully use all that this repository contains, you need to:

1. Download the Udacity Self-driving car simulator at [Link to Simulator Repo](https://github.com/udacity/self-driving-car-sim)

2. This repository is build on Python 2.7, PyTorch 0.3 and other packages that come directly with a quick miniconda installation. 
   To run the automatic driving however, several other packages are required. If missing, simply `conda install <required_package>`. 

---
	
The structure of this repository looks something like:

#### Folders:

1. `Data`:		The data that was used for training and testing. Contains images from driving on the sea and jungle track twice in each direction, respectively.

2. `Plots`:		Primarily plots that were used for the project presentations. Contains feature visualisation videos, angle distributions and more.

3. `Results`:	Contains all trained network weights and logging files to fully replicate everything we did.

4. `Training Bash Files`: Bash files used to train different runs under different conditions subsequently.

5. `fantastic`, `fastest` and `good`: Some random images highlighting the difference between driving on a low resolution vs. a high resolution road. Not relevant for any replication.

6. `DrivingVideos`: Contains three videos demonstrating the autonomous driving behaviour. `Road1_Runner` was trained on sea track and run on sea track. `Road2_DoubleRunner` was trained on jungle track and run on jungle track and `Road1_DoubleRunner` was trained on jungle track and run on sea track. 
![Demo DrivingVideo](/DrivingVideos/Road1_Runner.gif)
#### Scripts:
There are several files that need explaining, namely: 

1. `prediction_network.py`_(Karsten, Elias)_: 	This script contains the modular network classed we used to build our PyTorch Network.   

2. `helper_functions.py` _(Karsten, Elias)_:		Utility functions that are used in most other scripts. Contains the Training PyTorch Dataset, Logging utilities and other stuff.  

3. `training.py` _(Karsten, Elias)_: 				Training script to fully train the specified network structure.  

4. `run_automatic_driving.py` _(Karsten, Elias)_:	Script performing communication with localhost set up by the Udacity Simulator. Runs the network-based driving.   

5. `summarize.py, test_performance_test.py` _(Karsten)_: Use these scripts to summarize the training runs by learning curves and other summaries and check the performance on test-sets of choice.  

6. `renamer.py, make_relative_path.py` _(Karsten)_: Quality of life functions for data handling. Not directly relevant for the actual driving and training.  

7. `make_videos.py` _(Karsten)_:			Convert saved feature map data to nice movie frames.   

---

Examples on how to run each respective script from command line and useful arguments that can be passed can be shown by typing `python <script_name> --help`. The most important ones are however shown here:

_Running network training (Note that you can omit most arguments if you want to run the standard network)_:
```
python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None 110 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_best_4 
				   --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.3 0.3 0.3 0.3 0.3 0 --get_test_perf
```

_Running automatic driving_:
```
python run_automatic_driving.py --cuda --base_folder <name_of_folder_where_you_save_training_information_to> --speed <car_driving_speed_0_to_30> --extract_features
```

_Summarize Training Runs_:
```
python summarize.py --log --inc_test_loss --inc_emd --set_baselines
                    --base_folder /home/karsten_dl/e-kar/Results/Minitest_Comparisons --save_plot here
```

_Test Test performance_:
```
python test_performance_tester.py --cuda --show_hists --save
```