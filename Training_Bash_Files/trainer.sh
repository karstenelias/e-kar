### Ablation Studies



echo "Baseline run..."
python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name baseline_all --get_test_perf;
echo "-----------"
echo "No shadow..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name baseline_all_no_shadow_1 --get_test_perf;
echo "-----------"
echo "Random selection for validation..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name baseline_all_no_shadow_random_select --use_random --get_test_perf;
echo "-----------"
echo "Baseline without augmentation..."
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_baseline_without_aug --get_test_perf;


echo "-----------"
echo "H110 center..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None 110 --gamma 0.2 --tau 30 --cat_name fullcon_no_shadow_h110 --get_test_perf;
echo "-----------"
echo "H180 center..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None 80 --gamma 0.2 --tau 30 --cat_name fullcon_no_shadow_h80 --get_test_perf;
echo "-----------"
echo "H130 center..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None 130 --gamma 0.2 --tau 30 --cat_name fullcon_no_shadow_h130 --get_test_perf;
echo "-----------"
echo "H110 no center..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None 110 --gamma 0.2 --tau 30 --cat_name fullcon_no_shadow_h110_no_center --no_center_crop --get_test_perf;



echo "-----------"
echo "Jitter only..."
python training.py --cuda --augment jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_jitter_only --get_test_perf;
echo "-----------"
echo "Angle Jitter only..."
python training.py --cuda --augment angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_angle_jitter_only --get_test_perf;
echo "-----------"
echo "Flipping only..."
python training.py --cuda --augment flip --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_flip_only --get_test_perf;
echo "-----------"
echo "Shadow only..."
python training.py --cuda --augment shadow --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_shadow_only --get_test_perf;



echo "-----------"
echo "Bs: 8..."
python training.py --cuda --augment angle_jitter --tvsplit 0.7 --all --bs 8 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_bs8 --get_test_perf;
echo "-----------"
echo "Bs: 128..."
python training.py --cuda --augment flip --tvsplit 0.7 --all --bs 128 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_bs128 --get_test_perf;
echo "-----------"
echo "Bs: 256..."
python training.py --cuda --augment shadow --tvsplit 0.7 --all --bs 256 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_bs256 --get_test_perf;



#Testing various network structures - done
echo "Testing various network structures..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --n_filters 48 96 128 196 256 --use_BN;
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --kernel_sizes 10 7 5 3 3 --strides 4 3 2 2 1 --use_BN;
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --fc_layers 1024 500 100 10 --use_BN;

echo "-----------"
echo "Struct..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --kernel_sizes 10 7 5 3 3 --strides 4 3 2 2 1 --cat_name fullcon_bigger_kernels --get_test_perf;
echo "-----------"
echo "Struct..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --n_filters 48 96 128 196 256 --cat_name fullcon_more_filters --get_test_perf;
echo "-----------"
echo "Struct..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --fc_layers 1024 500 100 10 --cat_name fullcon_more_fullcon_neurons --get_test_perf;


#Checking the relevance of using --all - done
echo "Testing Relevance of --all"
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --bs 64 --n_epochs 300 --lr 0.001 --use_BN;
echo "-----------"
echo "Struct..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --bs 64 --n_epochs 70 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_no_all --get_test_perf;


#FC sizes - done
echo "Increasing FC size..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --fc_layers 2048 1024 500 100 10 --fc_dropout 0.3 0.3 0.3 0.2 0 --use_BN;


#Testing various dropout values - done
echo "Testing relevance of massive dropout..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --use_BN  --conv_dropout 0.5 0.5 0.3 0.3 0 --fc_dropout 0.5 0.5 0.3 0;

#Effects of different activations - done
echo "Testing various activations..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --use_BN --conv_activation elu --fc_activation elu;
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --use_BN --conv_activation prelu --fc_activation prelu;
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --use_BN --conv_activation leaky --fc_activation leaky;

#Massive Size
echo "Increasing full size..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --n_filters 24 36 48 64 64 128 --kernel_sizes 5 5 3 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.3 0.2 0.2 0.2 0.2 0 --fc_layers 2048 1024 500 100 10 --fc_dropout 0.3 0.3 0.3 0.2 0 --fc_layers 2048 1024 500 100 10 --fc_dropout 0.3 0.3 0.3 0.2 0 --use_BN;

#More Convs
echo "Increasing conv size..."
python training.py --cuda --augment flip jitter angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 300 --lr 0.001 --n_filters 24 36 48 64 64 128 --kernel_sizes 5 5 3 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.3 0.2 0.2 0.2 0.2 0 --use_BN;
