# echo "-----------"
# echo "Fully-Connected max 1"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_random --use_BN --force_shape None 110 --no_center_crop --gamma 0.2 --tau 30 --cat_name fullcon_best1 --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 1 1 1 --conv_dropout 0.3 0.3 0.3 0.3 0.3 0 --fc_dropout 0.3 0.3 0.3 0 --get_test_perf;
# echo "-----------"
# echo "Fully-Convolutional max 1"
# python training.py --cuda --augment flip jitter --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_1 --make_full_conv --get_test_perf --n_filters 24 36 48 64 64 92 128 --kernel_sizes 5 5 3 3 3 3 3 --strides 2 2 2 1 1 1 1 --conv_dropout 0.2 0.2 0.3 0.3 0.2 0.1 0;
# echo "-----------"
# echo "Fully-Convolutional max 3"
# python training.py --cuda --augment flip jitter --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_3 --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.2 0.2 0.2 0.2 0.2 0 --get_test_perf;
# echo "-----------"
# echo "Fully-Convolutional max 4"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_random --use_BN --force_shape None 110 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_best_4 --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.3 0.3 0.3 0.3 0.3 0 --get_test_perf;
# echo "-----------"
# echo "Fully-Convolutional max 2"
# python training.py --cuda --augment flip jitter --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_2 --make_full_conv --get_test_perf --n_filters 36 50 80 90 90 120 --kernel_sizes 5 5 3 3 3 3 --strides 2 2 2 1 1 1 --conv_dropout 0.2 0.2 0.3 0.3 0.2 0;
# echo "-----------"
# echo "Fully-Convolutional max 5"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_5 --make_full_conv --get_test_perf --n_filters 24 36 48 64 64 92 128 --kernel_sizes 5 5 3 3 3 3 3 --strides 2 2 2 1 1 1 1 --conv_dropout 0.2 0.2 0.3 0.3 0.2 0.1 0;
# echo "-----------"
# echo "Fully-Convolutional max 6"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None 120 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_best_6 --make_full_conv --get_test_perf --n_filters 24 36 48 64 64 92 128 --kernel_sizes 5 5 3 3 3 3 3 --strides 2 2 2 1 1 1 1 --conv_dropout 0.2 0.2 0.3 0.3 0.2 0.1 0;
# echo "-----------"
# echo "Fullycon max 2"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None None --no_center_crop --gamma 0.2 --tau 30 --cat_name fullcon_best_2 --get_test_perf --n_filters 24 36 48 64 64 92 128 --kernel_sizes 5 5 3 3 3 3 3 --strides 2 2 2 1 1 1 1 --conv_dropout 0.2 0.2 0.3 0.3 0.2 0.1 0 --fc_layers 100 --fc_dropout 0;
# echo "-----------"
# echo "Fullycon max 3"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_BN --force_shape None 120 --no_center_crop --gamma 0.2 --tau 30 --cat_name fullcon_best_3 --get_test_perf --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.2 0.2 0.2 0.2 0.2 0 --fc_layers 100 10 --fc_dropout 0.4 0;
# echo "-----------"
# echo "Fullycon max 4"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_BN --force_shape None 120 --no_center_crop --gamma 0.2 --tau 30 --cat_name fullcon_best_4 --get_test_perf --n_filters 24 36 48 64 64 --kernel_sizes 5 5 3 3 3  --strides 2 2 2 2 1 --conv_dropout 0.2 0.2 0.2 0.2 0 --fc_layers 200 100 10 --fc_dropout 0.4 0.4 0;
# echo "-----------"
# echo "Fully-Convolutional max 7"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.003 --use_BN --force_shape None 120 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_best_6 --make_full_conv --get_test_perf --n_filters 50 80 110 140 140 140 --kernel_sizes 5 3 3 3 3 3 --strides 1 1 1 1 1 1 --use_pool --pools 1 0 0 0 0 0 --conv_dropout 0.2 0.2 0.3 0.3 0.1 0;
# echo "-----------"
# echo "Road2 Runner"
# python training.py --cuda --augment flip jitter --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_random --use_BN --force_shape None 120 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_best_road2 --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.3 0.3 0.3 0.3 0.3 0 --get_test_perf --image_folders Road2_bf Road2_fb --test_image_folders Road1_bf Road1_fb;

# echo "-----------"
# echo "Road2 to 1 - 1"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_road3 --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.4 0.4 0.3 0.3 0.2 0 --get_test_perf --image_folders Road2_bf Road2_fb --test_image_folders Road1_bf Road1_fb;
# echo "-----------"
# echo "Road2 to 1 - 2"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_road9 --make_full_conv --n_filters 24 36 48 64 64 --kernel_sizes 5 5 3 3 3  --strides 2 2 1 1 1 --conv_dropout 0.2 0.2 0.2 0.2 0 --get_test_perf --image_folders Road2_bf Road2_fb --test_image_folders Road1_bf Road1_fb;
# echo "-----------"
# echo "Road2 to 1 - 3"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_road5 --n_filters 24 36 48 64 64 --kernel_sizes 5 5 3 3 3  --strides 2 2 1 1 1 --conv_dropout 0.4 0.4 0.3 0.3 0 --fc_dropout 0.5 0.3 0.3 0 --get_test_perf --image_folders Road2_bf Road2_fb --test_image_folders Road1_bf Road1_fb;
# echo "-----------"
# echo "Road2 to 1 - 4"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_best_road10 --conv_dropout 0.3 0.3 0.2 0.2 0 --fc_dropout 0.5 0.5 0.3 0 --get_test_perf --image_folders Road2_bf Road2_fb --test_image_folders Road1_bf Road1_fb;
# echo "-----------"
# echo "Road2 Runner V2"
# python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_random --use_BN --force_shape None 100 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_road_runner_new --make_full_conv --n_filters 24 36 64 92 128 --kernel_sizes 5 5 5 3 3 --strides 2 2 2 1 1 --conv_dropout 0.2 0.2 0.2 0.2 0 --get_test_perf --image_folders Road2_bf Road2_fb --test_image_folders Road1_bf Road1_fb;
echo "-----------"
echo "Road1 to 2"
python training.py --cuda --augment flip jitter --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_random --use_BN --force_shape None 105 --angle_weight -5 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_best_road2 --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 1 1 1 --conv_dropout 0.3 0.3 0.3 0.3 0.3 0 --get_test_perf --test_image_folders Road2_bf Road2_fb --image_folders Road1_bf Road1_fb;
#
# echo "-----------"
# echo "Road1 to 2"
# python training.py --cuda --augment flip jitter --tvsplit 0.7 --all --bs 8 --n_epochs 100 --lr 0.001 --use_random --use_BN --force_shape None 125 --angle_weight 5 --no_center_crop --gamma 0.2 --tau 30 --cat_name full_conv_best_road2 --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 1 1 1 --conv_dropout 0.3 0.3 0.3 0.3 0.3 0 --get_test_perf --test_image_folders Road2_bf Road2_fb --image_folders Road1_bf Road1_fb;
