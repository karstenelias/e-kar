echo "-----------"
echo "Fully-Connected baseline"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_baseline --get_test_perf;
echo "-----------"
echo "Fully-Connected massive reg"
python training.py --cuda --augment flip jitter angle_jitter shadow  --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_baseline_massreg --conv_dropout 0.3 0.3 0.2 0.2 0 --fc_dropout 0.3 0.3 0.3 0 --get_test_perf;


echo "-----------"
echo "Fully-Convolutional baseline"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_baseline --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional higher learning rate..."
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.002 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name baseline_lr2em3 --get_test_perf --make_full_conv;
echo "-----------"
echo "Fully-Convolutional no shadow lower learning rate..."
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.00008 --use_BN --force_shape None None --gamma 0.5 --tau 40 --cat_name baseline_lr8em5 --get_test_perf --make_full_conv;


echo "-----------"
echo "Fully-Convolutional wit fAt kernels "
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_fat_kernels --make_full_conv --kernel_sizes 10 7 5 3 3 --strides 3 2 2 2 1 --get_test_perf;
echo "-----------"
echo "Fully-Convolutional small kernels"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_small_kernels_small_strides --make_full_conv --kernel_sizes 5 3 3 3 3 --strides 2 1 1 1 1 --get_test_perf;
echo "-----------"
echo "Fully-Convolutional more filter"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_more_filters --make_full_conv --n_filters 48 96 128 196 224 --get_test_perf;
echo "-----------"
echo "Fully-Convolutional deeper"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_deeper --make_full_conv --n_filters 24 36 48 64 64 92 --kernel_sizes 5 5 5 3 3 3 --strides 2 2 2 2 1 1 --conv_dropout 0.1 0.1 0.1 0.1 0.1 0 --get_test_perf;
echo "-----------"
echo "Fully-Convolutional prelu"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_prelu --make_full_conv --conv_activation prelu --get_test_perf;
echo "-----------"
echo "Fully-Convolutional more drop"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_more_drop --make_full_conv --conv_dropout 0.4 0.4 0.4 0.4 0 --get_test_perf;
echo "-----------"
echo "Fully-Convolutional medium drop"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_medium_drop --make_full_conv --conv_dropout 0.3 0.3 0.2 0.2 0 --get_test_perf;
echo "-----------"
echo "Fully-Convolutional less drop"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_less_drop --make_full_conv --conv_dropout 0.1 0.1 0.1 0.1 0 --get_test_perf;


echo "-----------"
echo "Fully-Convolutional smaller height"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None 110 --gamma 0.2 --tau 30 --cat_name full_conv_h110 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional ch3"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --channels 3 --cat_name full_conv_ch3 --use_random --make_full_conv --get_test_perf;


echo "-----------"
echo "Fully-Convolutional h110 center"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None 110 --gamma 0.2 --tau 30 --cat_name full_conv_h110 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional h110 no center"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None 110 --gamma 0.2 --tau 30 --no_center_crop --cat_name full_conv_no_center_crop_h110 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional h130 no center"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None 130 --gamma 0.2 --tau 30 --no_center_crop --cat_name full_conv_h130 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional h80 no center "
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None 80 --gamma 0.2 --tau 30 --cat_name full_conv_h80 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional no center h110 full aug"
python training.py --augment flip jitter shadow --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None 110 --gamma 0.2 --tau 30 --no_center_crop --cat_name full_conv_no_center_crop_h110_full_aug --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Con h110 no center"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None 110 --gamma 0.2 --tau 30 --no_center_crop --cat_name fullcon_no_center_crop_h110 --get_test_perf;



echo "-----------"
echo "Fully-Convolutional shorter more filters"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_shorter_more_filters --make_full_conv --n_filters 24 36 64 128 --kernel_sizes 5 5 3 3 --strides 2 2 2 1 --conv_dropout 0 0 0 0 --get_test_perf;
echo "-----------"
echo "Fully-Convolutional higher tv split"
python training.py --cuda --tvsplit 0.5 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_tv05 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional lower tv split"
python training.py --cuda --tvsplit 0.9 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_tv09 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional higher tv split max augment"
python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.5 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_tv05_full_augment --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional only center images"
python training.py --cuda --tvsplit 0.7 --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_only_center --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional lower tv split"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_use_random --use_random --make_full_conv --get_test_perf;


echo "-----------"
echo "Fully-Convolutional flipping"
python training.py --cuda --augment flip --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_flip --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional jitter"
python training.py --cuda --augment jitter --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_jitter --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional angle jitter"
python training.py --cuda --augment angle_jitter --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_angle_jitter --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional shadow"
python training.py --cuda --augment shadow --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_shadow --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional flipping & Jitter"
python training.py --cuda --augment flip jitter --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_flip_and_jitter --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional full augmentation"
python training.py --cuda --augment flip jitter angle_jitter shadow --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_full_aug --make_full_conv --get_test_perf;


echo "-----------"
echo "Fully-Convolutional bs 8"
python training.py --cuda --tvsplit 0.7 --all --bs 8 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_bs8 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional bs 128"
python training.py --cuda --tvsplit 0.7 --all --bs 128 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_bs128 --make_full_conv --get_test_perf;
echo "-----------"
echo "Fully-Convolutional bs 256"
python training.py --cuda --tvsplit 0.7 --all --bs 256 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name full_conv_bs256 --make_full_conv --get_test_perf;


echo "-----------"
echo "Fully-Connected mini"
python training.py --cuda --tvsplit 0.7 --all --bs 64 --n_epochs 80 --lr 0.0003 --use_BN --force_shape None None --gamma 0.2 --tau 30 --cat_name fullcon_baseline_mini --fc_layers 100 10 --fc_dropout 0 0 --get_test_perf;
