"""
The purpose of this file is to simple add absolute image paths to the training CSV.
"""

import pandas as pd

filepath = "/home/karsten_dl/AI_Project/Data/Road1_fb/"
csv_file = pd.read_csv(filepath+"driving_log.csv", header=None)
csv_file[0] = ["/".join(y for y in x.split("/")[-4:]) for x in csv_file[0]]
csv_file[1] = ["/".join(y for y in x.split("/")[-4:]) for x in csv_file[1]]
csv_file[2] = ["/".join(y for y in x.split("/")[-4:]) for x in csv_file[2]]

csv_file.to_csv(filepath+"driving_log.csv", index=False, header=None)
