import os
import numpy as np

pth = os.getcwd()+"/Results/"
fls = os.listdir(pth)
for i,nm in enumerate(fls):
    new_name = input("[{}/{}] Rename {} to:   ".format(i,len(fls),nm))
    weight_name = np.array(os.listdir(pth+nm))[["NVIDIA" in x for x in os.listdir(pth+nm)]][0]
    os.rename(pth+nm+"/"+weight_name, pth+nm+"/"+new_name)
    os.rename(pth+nm, pth+nm.split("_")[0]+"_net_"+new_name)
    # os.rename(pth+nm+"/"+np.array(os.listdir(pth+nm))[["NVIDIA" in x for x in os.listdir(pth+nm)]][0],pth+nm+"/"+nm)
